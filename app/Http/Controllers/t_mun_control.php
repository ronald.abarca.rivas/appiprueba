<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\t_mun;

class t_mun_control extends Controller
{

    public function index()
    {
        $request=new t_mun();
        return $request::all();
    }


    public function create(Request $request)
    {
        $t_mun=new t_mun();
        $t_mun->m_name=$request->post('m_name');
        $t_mun->md_id=$request->post('md_id');
        $t_mun->save();
        return $t_mun;
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request)
    {
        $nombre=$request->m_name;
        $mdid=$request->md_id;
        $t_mun=t_mun::where("m_id","=",$id)
        ->update(['m_name'=>$nombre,'md_id'=>4]);
        return $t_mun;
    }


    public function destroy($id)
    {
        //
    }
}
