<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\t_dep;



class t_dep_control extends Controller
{

    public function index()
    {
        $request=new t_dep();
        return $request::all();
    }


    public function create(Request $request)
    {
        $t_dep=new t_dep();
        $t_dep->d_name=$request->post('d_name');
        $t_dep->save();
        return $t_dep;
    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request)
    {
        $nombre=$request->d_name;
        $d_id=$request->d_id;
        $t_dep=t_dep::where("d_id","=",$d_id)
        ->update(['d_name'=>$nombre]);

        return $t_dep;
        //return t_dep::where("d_id","=",$d_id)->get();

        /*{
            "d_id":3,
            "d_name":"asdad1"

        }*/

    }


    public function destroy($d_id)
    {
        $t_dep=t_dep::where("d_id","=",$d_id)->delete();
        return $t_dep;

    }
}
