<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\t_productRep;

class t_productRep_control extends Controller
{
    /*public function index()
    {
        $request=new t_productRep();
        return $request::all();
    }*/

    public function index(Request $request){
        $buscar=trim($request->post("buscar"));
        $opiniones=t_productRep::join('t_product','t_productRep.pr_prodid',"=","t_product.p_id")
                    ->join('t_farm','t_productRep.pr_farmid',"=","t_farm.f_id")
                    ->join('t_category','t_product.p_category',"=",'t_category.c_id')
                    ->select("*")
                    ->where("p_name","LIKE","%".$buscar."%")
                    ->get();
        //return compact('opiniones',"buscar");
        return $opiniones;
    }

    public function index2($c_id)
    {
        $t_productRep=t_productRep::where("c_id","=",$c_id)->get();
        return $t_productRep;

    }


    public function create(Request $request)
    {
        $t_productRep=new t_productRep();
        $t_productRep->d_name=$request->post('d_name');
        $t_productRep->save();
        return $t_productRep;
    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request)
    {
        $nombre=$request->d_name;
        $d_id=$request->d_id;
        $t_productRep=t_productRep::where("d_id","=",$d_id)
        ->update(['d_name'=>$nombre]);

        return $t_productRep;
        //return t_productRep::where("d_id","=",$d_id)->get();

        /*{
            "d_id":3,
            "d_name":"asdad1"

        }*/

    }


    public function destroy($d_id)
    {
        $t_productRep=t_productRep::where("d_id","=",$d_id)->delete();
        return $t_productRep;

    }
}
