<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\t_category;

class t_category_control extends Controller
{
    public function index()
    {
        $request=new t_category();
        return $request::all();
    }

    public function index2($c_id)
    {
        $t_category=t_category::where("c_id","=",$c_id)->get();
        return $t_category;

    }


    public function create(Request $request)
    {
        $t_category=new t_category();
        $t_category->d_name=$request->post('d_name');
        $t_category->save();
        return $t_category;
    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request)
    {
        $nombre=$request->d_name;
        $d_id=$request->d_id;
        $t_category=t_category::where("d_id","=",$d_id)
        ->update(['d_name'=>$nombre]);

        return $t_category;
        //return t_category::where("d_id","=",$d_id)->get();

        /*{
            "d_id":3,
            "d_name":"asdad1"

        }*/

    }


    public function destroy($d_id)
    {
        $t_category=t_category::where("d_id","=",$d_id)->delete();
        return $t_category;

    }

}
