<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class t_productRep extends Model
{
    protected $table="t_productRep";
    public $timestamps=false;
}
