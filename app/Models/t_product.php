<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class t_product extends Model
{
    protected $table="t_product";
    public $timestamps=false;
}
