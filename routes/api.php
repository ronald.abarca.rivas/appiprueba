<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\controllers\t_dep_control;
use App\Http\controllers\t_mun_control;
use App\Http\controllers\t_category_control;
use App\Http\controllers\t_product_control;
use App\Http\controllers\t_productRep_control;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/t_dep', [t_dep_control::class,"index"]);
Route::post('/t_dep', [t_dep_control::class,"create" ]);
Route::put('/t_dep', [t_dep_control::class,"update" ]);
Route::delete('/t_dep/{id}', [t_dep_control::class,"destroy" ]);

Route::get('/t_mun', [t_mun_control::class,"index"]);
Route::post('/t_mun', [t_mun_control::class,"create" ]);
Route::put('/t_mun', [t_mun_control::class,"update" ]);
Route::delete('/t_mun/{id}', [t_mun_control::class,"destroy" ]);

Route::get('/t_category', [t_category_control::class,"index"]);
Route::get('/t_category/{id}', [t_category_control::class,"index2" ]);


Route::get('/t_product', [t_product_control::class,"index"]);



Route::get('/t_productRep', [t_productRep_control::class,"index"]);
